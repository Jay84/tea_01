<?php 

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Item;

class ItemTransformer extends TransformerAbstract
{
	public function transform(Item $item)
	{
		return [
			"id" => $item->id,
			"name" => $item->name,
			"description" => $item->description,
			"image" => $item->img_path,
			"price" => $item->price,
			"price10" => $item->price10,
			"price50" => $item->price50,
			"organic" => $item->organic,
			"available" => $item->available,
			"img_path" => $item->img_path,
		];
	}
}
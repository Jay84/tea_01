<?php

namespace App\Providers;

use App\Models\Category;
use App\Models\Type;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\ServiceProvider;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer("front.shop.index", function($view){

            $sidebar_items = Category::all()->reduce(function($carry, $category){
                $carry[] = array(
                        "id" => $category->id,
                        "name" => $category->name,
                        "slug" => $category->slug,
                        "types" => $category->types->reduce(function($carry, $type){
                            $carry[] = [
                            "id" => $type->id,
                            "name" => $type->name, 
                            "slug" => $type->slug,
                            "showSections" => false,
                            "sections" => $type->sections->reduce(function($carry, $section){
                                $carry[] = [
                                    "id" => $section->id,
                                    "name" => $section->name, 
                                    "slug" => $section->slug,
                                    "subsections" => $section->subsections->reduce(function($carry, $subsection){
                                        $carry[] = [
                                            "id" => $subsection->id,
                                            "name" => $subsection->name, 
                                            "slug" => $subsection->slug,
                                        ];
                                        return $carry;
                                    }, [])
                                ];
                                return $carry;
                            }, [])
                            ];
                            return $carry;
                        }, [])
                    );
                return $carry;
            }, []);
                
            $view->with("sidebar_items", $sidebar_items);

        });

            view()->composer("layouts.app", function($view) {
                if (Session::has("cart")) {
                    $cart_items_count = Session::get("cart")->sum("qty");
                } else {
                    $cart_items_count = 0;
                }
                $view->with("cart_items_count", $cart_items_count);
            });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

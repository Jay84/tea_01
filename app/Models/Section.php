<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    public function subsections()
    {
    	return $this->hasMany(Subsection::class, "parent_id");
    }

    public function type()
    {
    	return $this->belongsTo(Type::class, "parent_id");
    }
}

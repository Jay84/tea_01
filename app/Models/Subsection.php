<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subsection extends Model
{
    public function section()
    {
    	return $this->belongsTo(Section::class, "parent_id");
    }
}

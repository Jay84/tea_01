<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
	protected $fillable = [
		"order_id", "item_id", "quantity", "price", "unit", "user_id"
	];

    public function order()
    {
    	return $this->belongsTo(Order::class);
    }

    public function item()
    {
    	return $this->belongsTo(Item::class);
    }
}

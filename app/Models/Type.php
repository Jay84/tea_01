<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    public function sections()
    {
    	return $this->hasMany(Section::class, "parent_id");
    }

    public function category()
    {
    	return $this->belongsTo(Category::class, "parent_id");
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;

class Cart extends Model
{
	protected $cart;

	public function __construct()
	{
		$this->prepareCart();
	}

	protected function prepareCart()
	{
		if (Session::has("cart")) {
			$this->cart = Session::get("cart");
		} else {
			Session::put(["cart" => collect()]);
		}

		$this->cart = Session::get("cart");
	}

	public function addToCart(Item $item, $qty, $priceType)
	{
		if ($this->alreadyInCart($item, $priceType)) {
			
			$this->incrementByQuantity($item, $qty, $priceType)->pushToSession();
			
		} else {
			
			$new_item = $this->createNewItem($item, $qty, $priceType);
			$this->pushToCart($new_item)->pushToSession();
		}
		
	}

	// public function removeItem(Item $item, $priceType)
	// {
	// 	$this->cart = $this->cart->filter(function($current_item) use ($item, $priceType) {
	// 		return !($current_item["id"] === $item->id && $current_item["price"] === $item->{$priceType});
	// 	});

	// 	$this->pushToSession();

	// }
	
	public function removeOneItem(Item $item, $qty, $priceType)
	{
		$this->cart = $this->cart->map(function($cart_item) use ($item, $qty, $priceType){
			if ($cart_item["id"] === $item->id && $cart_item["price"] === $item->{$priceType}) {
				$cart_item["qty"]-= $qty;
			}
			return $cart_item;
		});
		
		$this->pushToSession();
	}
	
	public function getItem(Item $item, $priceType)
	{
		return $this->cart->first(function($cart_item) use ($item, $priceType) {
			return ($cart_item["id"] === $item->id && $cart_item["price"] === $item->{$priceType});
		});
		
	}

	public function getCartItemsCount()
	{
		return $this->cart->sum("qty");
	}

	protected function createNewItem(Item $item, $qty, $priceType)
	{
		$price_unit = $this->getPriceUnit($item, $priceType);
		
		return $new_item = [
				"id" => $item->id,
				"name" => $item->name,
				"description" => $item->description,
				"price" => $item->{$priceType},
				"priceType" => $priceType,
				"unit" => $price_unit,
				"qty" => $qty,
				"img_path" => $item->img_path
		];
	}

	protected function getPriceUnit(Item $item, $priceType)
	{
		if ($item->category_id !== 1) {
			return "Ks";
		}
		if ($priceType === "price") {
			return "100g";
		}
		if ($priceType === "price50") {
			return "50g";
		}
		if ($priceType === "price10") {
			return "10g";
		}

	}

	protected function incrementByQuantity(Item $item, $qty, $priceType)
	{
		$this->cart = $this->cart->map(function($cart_item) use ($item, $qty, $priceType){
			if ($cart_item["id"] === $item->id && $cart_item["price"] === $item->{$priceType}) {
				$cart_item["qty"]+= $qty;
			}
			return $cart_item;
		});

		return $this;
	}
	
	protected function alreadyInCart(Item $item, $priceType) {
		
		return $this->cart->contains(function($cart_item) use ($item, $priceType){
			return ($cart_item["id"] === $item->id && $cart_item["price"] === $item->{$priceType});
		});
	}

	protected function pushToCart($cart_item)
	{
		$this->cart->push($cart_item);

		return $this;
	}

	protected function pushToSession()
	{
		Session::put(["cart" => $this->cart]);
	}






    
}

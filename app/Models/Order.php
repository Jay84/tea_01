<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
	protected $fillable = [
		"name", "street", "city", "psc", "phone", "email", "country", "company", 
		"fax", "ico", "dic", "", "contact", "message", "name_second", "street_second", 
		"city_second", "psc_second", "country_second", "company_second"
	];

    public function purchases()
    {
    	return $this->hasMany(Purchase::class);
    }
}

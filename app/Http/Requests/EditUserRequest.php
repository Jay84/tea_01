<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class EditUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $userId = Auth::user()->id;
        return [
            "company" => "required|max:255",
            "first_name" => "required|max:255",
            "last_name" => "required|max:255",
            "address" => "required|max:255",
            "city" => "required|max:255",
            "psc" => "required|max:255",
            "phone" => "required|max:255",
            "email" => "required|max:255|email|confirmed|unique:users,email," . $userId,
            "name" => "required|max:255",
        ];
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegistrationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "company" => "required|max:255",
            "customer_type" => "required|max:255|in:maloobchod,velkoobchod,slovensko",
            "first_name" => "required|max:255",
            "last_name" => "required|max:255",
            "address" => "required|max:255",
            "city" => "required|max:255",
            "psc" => "required|max:255",
            "phone" => "required|max:255",
            "email" => "required|max:255|email|confirmed|unique:users",
            "name" => "required|max:255",
            "password" => "required|min:6|confirmed",
        ];
    }
}

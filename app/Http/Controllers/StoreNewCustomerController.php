<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegistrationRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class StoreNewCustomerController extends Controller
{
        public function store(RegistrationRequest $request)
        {
        	$request->has("news") ? $news = true : $news = false;
        	
        	User::create([
    			"company" => $request->company,
    			"ico" => $request->ico,
    			"dic" => $request->dic,
    			"first_name" => $request->first_name,
    			"last_name" => $request->last_name,
    			"address" => $request->address,
    			"city" => $request->city,
    			"psc" => $request->psc,
    			"phone" => $request->phone,
    			"fax" => $request->fax,
    			"email" => $request->email,
    			"www" => $request->www,
    			"name" => $request->name,
    			"password" => bcrypt($request->password),
    			"message" => $request->message,
    			"customer_type" => $request->customer_type,
    			"news" => $news
    		]);

            Session::flash("status", "Registrace úspěšně dokončena!");
    		return redirect("/login");
        }
}

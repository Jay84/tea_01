<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Item;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CartController extends Controller
{
    public function add(Request $request)
    {
    	$this->validate($request, [
    		"id" => "required|integer|min:1",
            "qty" => "required|integer|min:1",
    		"priceType" => "required"
		]);

		$cart = new Cart();
		$item = Item::findOrFail($request->id);

		$cart->addToCart($item, $request->qty, $request->priceType);
        $cart_items_count = $cart->getCartItemsCount();

    	return response()->make(["cart_items_count" => $cart_items_count], 200);
    }

    public function substract(Request $request)
    {
        $this->validate($request, [
            "id" => "required|integer|min:1",
            "qty" => "required|integer|min:1",
            "priceType" => "required"
        ]);

        $cart = new Cart();
        $item = Item::findOrFail($request->id);

        $cart->removeOneItem($item, $request->qty, $request->priceType);
        
        $cart_items_count = $cart->getCartItemsCount();

        return response()->make(["cart_items_count" => $cart_items_count], 200);
    }

    public function remove(Request $request)
    {

        Session::put(["cart" => collect($request->all())]);

       return response()->make(["cart_items_count" => Session::get("cart")->sum("qty")]);

    }
}

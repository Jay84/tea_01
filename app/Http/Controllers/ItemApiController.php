<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Item;
use App\Models\Section;
use App\Models\Subsection;
use App\Models\Type;
use App\Transformers\ItemTransformer;
use Illuminate\Http\Request;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class ItemApiController extends Controller
{
    protected $perPage = 12;

    public function category(Category $category)
    {
    	!$category->exists ? $items = Item::paginate($this->perPage) : $items = Item::where("category_id", $category->id)->paginate($this->perPage);
    	
    	return $this->prepareResponse($items);
    }

    public function type(Category $category, Type $type)
    {
		return	$items = $this->collectItems($category, $type);    	
    }

    public function section(Category $category, Type $type, Section $section)
    {
    	return	$items = $this->collectItems($type, $section);
    }

    public function subsection(Category $category, Type $type, Section $section, Request $request)
    {
        $ids = array_except($request->all(), "page");
        $ids = array_values($ids);
    	$items = Item::whereIn("subsection_id", $ids)->paginate($this->perPage);
        
        return $this->prepareResponse($items);
    }

    public function collectItems($parent, $child)
    {
    	$parentClass = $this->getClassName($parent);
    	$childClass = $this->getClassName($child);

    	if (!$child->exists){
    		$items = Item::where($parentClass . "_id", $parent->id)->paginate($this->perPage);
    	} else {
    		$items = Item::where($childClass . "_id", $child->id)->paginate($this->perPage);
    	}

		return $this->prepareResponse($items);
    }

    public function getClassName($model)
    {
    	$name = get_class($model);
    	$name = explode("\\", $name);

    	return array_pop($name);
    }

    public function prepareResponse($items)
    {
    	return fractal()
    		->collection($items)
    		->transformWith(new ItemTransformer)
    		->paginateWith(new IlluminatePaginatorAdapter($items))
    		->toArray();
    }
}

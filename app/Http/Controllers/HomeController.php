<?php

namespace App\Http\Controllers;

use App\Http\Requests\EditUserRequest;
use App\Models\Purchase;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        return view('user.home', compact("user"));
    }

    public function update(EditUserRequest $request)
    {
        $request->has("news") ? $news = true : $news = false;

        $user = Auth::user();
        $user->fill($request->all());
        $user->news = $news;

        $user->update();

        return back();
    }

    public function logout()
    {
        Auth::logout();

        return redirect("/");
    }

    public function orders()
    {
        $user = Auth::user();
        $purchases = Purchase::with("item")->where("user_id", $user->id)->get()->groupBy("order_id")->reverse();

        return view("user.orders", compact("purchases"));
    }
}

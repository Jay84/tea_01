<?php

namespace App\Http\Controllers\Registration;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegistrationRequest;
use App\User;
use Illuminate\Http\Request;

class WholesaleRegistrationController extends Controller
{
    public function create()
    {
    	return view("front.registration.wholesale")->with(["customer_type" => "velkoobchod"]);
    }
}

<?php

namespace App\Http\Controllers\Registration;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegistrationRequest;
use App\User;
use Illuminate\Http\Request;

class RetailRegistrationController extends Controller
{
    public function create()
    {
    	return view("front.registration.retail")->with(["customer_type" => "maloobchod"]);
    }
}

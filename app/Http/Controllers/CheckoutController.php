<?php

namespace App\Http\Controllers;

use App\Http\Requests\CheckoutRequest;
use App\Jobs\SendOrderShippedEmail;
use App\Mail\OrderReceived;
use App\Models\Order;
use App\Models\Purchase;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class CheckoutController extends Controller
{
    public function index()
    {
    	if (!Session::get("cart")) {
    		Session::put(["cart" => collect()]);
    	}

    	$cart = Session::get("cart");
    	$user = $this->getUserInfo();

    	$input = [$cart, $user];

    	return view("front.checkout.overview", compact("input"));
    }

    public function getUserInfo()
    {
    	if ($user = Auth::user()) {
	    	$info = [
	    		"phone" => $user->phone,
	    		"dic" => $user->dic,
	    		"city" => $user->city,
	    		"ico" => $user->ico,
	    		"company" => $user->company,
	    		"fax" => $user->fax,
	    		"email" => $user->email,
	    		"first_name" => $user->first_name,
	    		"last_name" => $user->last_name,
	    		"psc" => $user->psc,
	    		"address" => $user->address,
	    		"id" => $user->id,
	    	];
    	} else {
    		$info = [
	    		"phone" => "",
	    		"dic" => "",
	    		"city" => "",
	    		"ico" => "",
	    		"company" => "",
	    		"fax" => "",
	    		"email" => "",
	    		"first_name" => "",
	    		"last_name" => "",
	    		"psc" => "",
	    		"address" => "",
	    		"id" => null,
	    	];	
    	}
    	return $info;
    }

    public function store(CheckoutRequest $request)
    {
    	$order = Order::create([
		    		"name" => $request->address["name"],
		    		"street" => $request->address["street"],
		    		"city" => $request->address["city"],
		    		"psc" => $request->address["psc"],
		    		"phone" => $request->address["phone"],
		    		"email" => $request->address["email"],
		    		"country" => $request->address["country"],
		    		"company" => $request->address["company"],
		    		"fax" => $request->address["fax"],
		    		"ico" => $request->address["ico"],
		    		"dic" => $request->address["dic"],
		    		"contact" => $request->address["contact"],
		    		"message" => $request->address["message"],
		    		"name_second" => $request->addressSecondary["name"],
		    		"company_second" => $request->addressSecondary["company"],
		    		"street_second" => $request->addressSecondary["street"],
		    		"city_second" => $request->addressSecondary["city"],
		    		"psc_second" => $request->addressSecondary["psc"],
		    		"country_second" => $request->addressSecondary["country"],
				]);

		foreach ($request->items as $item) {
			Purchase::create([
				"order_id" => $order->id,
				"item_id" => $item["id"],
				"user_id" => $request->address["id"],
				"quantity" => $item["qty"],
				"price" => $item["price"],
				"unit" => $item["unit"],
			]);
		}

		Session::forget("cart");
		Session::flash("purchase", "completed");
		
		Mail::to("jezzi@centrum.cz")->queue(new OrderReceived);
    }
}

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/', 'ShopController@index');

Route::get('/kosik', 'CheckoutController@index');
Route::post('/kosik', 'CheckoutController@store');

Route::get('/maloobchod', 'Registration\RetailRegistrationController@create');
Route::get('/velkoobchod', 'Registration\WholesaleRegistrationController@create');
// Route::post('/velkoobchod', 'Registration\WholesaleRegistrationController@store');
Route::get('/slovensko', 'Registration\SlovakiaRegistrationController@create');
// Route::post('/slovensko', 'Registration\SlovakiaRegistrationController@store');
Route::post('/newcustomer', 'StoreNewCustomerController@store');

Route::post('/cart', 'CartController@add');
Route::post('/cart/remove', 'CartController@remove');
Route::post('/cart/substract', 'CartController@substract');


Route::get('/home', 'HomeController@index');
Route::get('/logout', 'HomeController@logout');
Route::put('/home', 'HomeController@update');
Route::get('/historie', 'HomeController@orders');

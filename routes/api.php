<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');


Route::group(["prefix" => "category"], function(){
	Route::get("/", "ItemApiController@category");
	Route::get("/{category}", "ItemApiController@category");
		
		Route::get("/{category}/type", "ItemApiController@type");
		Route::get("/{category}/type/{type}", "ItemApiController@type");
			
			Route::get("/{category}/type/{type}/section", "ItemApiController@section");
			Route::get("/{category}/type/{type}/section/{section}", "ItemApiController@section");
				
				Route::get("/{category}/type/{type}/section/{section}/subsection", "ItemApiController@subsection");
});			


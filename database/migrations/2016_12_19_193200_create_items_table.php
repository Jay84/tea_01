<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->increments('id');
            $table->string("name");
            $table->text("description")->nullable();
            $table->integer("price")->unsigned();
            $table->integer("price50")->nullable()->unsigned();
            $table->integer("price10")->nullable()->unsigned();
            $table->boolean("organic");
            $table->integer("category_id")->unsigned();
            $table->integer("type_id")->unsigned();
            $table->integer("section_id")->unsigned()->nullable();
            $table->integer("subsection_id")->unsigned()->nullable();
            $table->integer("available");
            $table->string("img_path");
            $table->timestamps();

        });

        Schema::table("items", function(Blueprint $table) {
            $table->foreign("category_id")->references("id")->on("categories")->ondelete("cascade");
            $table->foreign("type_id")->references("id")->on("types")->ondelete("cascade");
            $table->foreign("section_id")->references("id")->on("sections")->ondelete("cascade");
            $table->foreign("subsection_id")->references("id")->on("subsections")->ondelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}

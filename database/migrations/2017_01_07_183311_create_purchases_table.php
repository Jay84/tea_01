<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchases', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("item_id")->unsigned();
            $table->integer("order_id")->unsigned();
            $table->integer("user_id")->unsigned()->nullable();
            $table->integer("quantity");
            $table->integer("price");
            $table->string("unit");
            $table->timestamps();

        });

        Schema::table("purchases", function(Blueprint $table) {
            $table->foreign("order_id")->references("id")->on("orders");
            $table->foreign("user_id")->references("id")->on("users");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchases');
    }
}

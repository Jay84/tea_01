<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('email')->unique();
            $table->string('phone')->nullable();
            $table->string('address')->nullable();
            $table->string('city')->nullable();
            $table->string('psc')->nullable();
            $table->string('company')->nullable();
            $table->string('ico')->nullable();
            $table->string('dic')->nullable();
            $table->string('fax')->nullable();
            $table->string('www')->nullable();
            $table->string('name')->nullable();
            $table->string('password')->nullable();
            $table->string('message')->nullable();
            $table->string('customer_type')->nullable();
            $table->string('news')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

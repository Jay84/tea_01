<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('street');
            $table->string('city');
            $table->string('psc');
            $table->string('phone');
            $table->string('email');
            $table->string('country');
            $table->string('company');
            $table->string('fax');
            $table->string('ico');
            $table->string('dic');
            $table->string('contact');
            $table->string('message');
            $table->string('name_second');
            $table->string('street_second');
            $table->string('city_second');
            $table->string('psc_second');
            $table->string('country_second');
            $table->string('company_second');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}

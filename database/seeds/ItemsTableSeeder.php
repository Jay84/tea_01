<?php

use Crockett\CsvSeeder\CsvSeeder;

class ItemsTableSeeder extends CsvSeeder 
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seedFromCSV(public_path("/seeder/items.csv"), "items");
    }
}

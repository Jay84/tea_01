<?php

use App\Models\Subsection;
use Illuminate\Database\Seeder;

class SubsectionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('subsections')->delete();

        
        	Subsection::create(['parent_id' => "1", "name" => "First flush", "slug" => str_slug("First flush", "-")]);
        	Subsection::create(['parent_id' => "1", "name" => "Second flush", "slug" => str_slug("Second flush", "-")]);
        	Subsection::create(['parent_id' => "1", "name" => "Podzimní a směsy", "slug" => str_slug("Podzimní a směsy", "-")]);
        	Subsection::create(['parent_id' => "1", "name" => "Oolongy a zelené", "slug" => str_slug("Oolongy a zelené", "-")]);
        	Subsection::create(['parent_id' => "2", "name" => "Černý", "slug" => str_slug("Černý", "-")]);
        	Subsection::create(['parent_id' => "2", "name" => "Zelený", "slug" => str_slug("Zelený", "-")]);
        	Subsection::create(['parent_id' => "10", "name" => "Zelený typ - lisované", "slug" => str_slug("Zelený typ - lisované", "-")]);
        	Subsection::create(['parent_id' => "10", "name" => "Tmavý typ - lisované", "slug" => str_slug("Tmavý typ - lisované", "-")]);
           	Subsection::create(['parent_id' => "10", "name" => "Sypané", "slug" => str_slug("Sypané", "-")]);
           	Subsection::create(['parent_id' => "19", "name" => "Litina", "slug" => str_slug("Litina", "-")]);
           	Subsection::create(['parent_id' => "19", "name" => "Keramika", "slug" => str_slug("Keramika", "-")]);
           	Subsection::create(['parent_id' => "19", "name" => "Porcelán a sklo", "slug" => str_slug("Porcelán a sklo", "-")]);
           	Subsection::create(['parent_id' => "19", "name" => "Čajový obřad chanoyu", "slug" => str_slug("Čajový obřad chanoyu", "-")]);
           	Subsection::create(['parent_id' => "20", "name" => "Porcelán a sklo", "slug" => str_slug("Porcelán a sklo", "-")]);
        	Subsection::create(['parent_id' => "20", "name" => "Keramika", "slug" => str_slug("Keramika", "-")]);
    }
}

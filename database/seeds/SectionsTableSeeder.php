<?php

use App\Models\Section;
use Illuminate\Database\Seeder;

class SectionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sections')->delete();

        	Section::create(['parent_id' => "1", "name" => "Darjeeling", "slug" => str_slug("Darjeeling", "-")]);
        	Section::create(['parent_id' => "1", "name" => "Assam", "slug" => str_slug("Assam", "-")]);
        	Section::create(['parent_id' => "1", "name" => "Nepal", "slug" => str_slug("Nepal", "-")]);
        	Section::create(['parent_id' => "1", "name" => "Ostatní", "slug" => str_slug("Ostatní", "-")]);
        	Section::create(['parent_id' => "4", "name" => "Černé", "slug" => str_slug("Černé", "-")]);
        	Section::create(['parent_id' => "4", "name" => "Zelené", "slug" => str_slug("Zelené", "-")]);
        	Section::create(['parent_id' => "4", "name" => "Bílé", "slug" => str_slug("Bílé", "-")]);
        	Section::create(['parent_id' => "4", "name" => "Žluté", "slug" => str_slug("Žluté", "-")]);
        	Section::create(['parent_id' => "4", "name" => "Oolongy", "slug" => str_slug("Oolongy", "-")]);
        	Section::create(['parent_id' => "4", "name" => "Dodatečně fermentované", "slug" => str_slug("Dodatečně fermentované", "-")]);
        	Section::create(['parent_id' => "5", "name" => "Oolongy", "slug" => str_slug("Oolongy", "-")]);
        	Section::create(['parent_id' => "8", "name" => "Tradiční čínské", "slug" => str_slug("Tradiční čínské", "-")]);
        	Section::create(['parent_id' => "8", "name" => "Zelené", "slug" => str_slug("Zelené", "-")]);
        	Section::create(['parent_id' => "8", "name" => "Černé", "slug" => str_slug("Černé", "-")]);
        	Section::create(['parent_id' => "8", "name" => "Polofermentované a ostatní", "slug" => str_slug("Polofermentované a ostatní", "-")]);
        	Section::create(['parent_id' => "10", "name" => "Maté", "slug" => str_slug("Maté", "-")]);
        	Section::create(['parent_id' => "10", "name" => "Rooibos", "slug" => str_slug("Rooibos", "-")]);
            Section::create(['parent_id' => "10", "name" => "Jiné rostlinky a směsy", "slug" => str_slug("Jiné rostlinky a směsy", "-")]);
            Section::create(['parent_id' => "12", "name" => "Japonské", "slug" => str_slug("Japonské", "-")]);
            Section::create(['parent_id' => "12", "name" => "Čínské", "slug" => str_slug("Čínské", "-")]);
            Section::create(['parent_id' => "12", "name" => "Vietnamské", "slug" => str_slug("Vietnamské", "-")]);
        	Section::create(['parent_id' => "12", "name" => "Ostatní", "slug" => str_slug("Ostatní", "-")]);
    }
}

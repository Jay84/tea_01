<?php

use App\Models\Type;
use Illuminate\Database\Seeder;

class TypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('types')->delete();

        	Type::create(['name' => "Indické", "parent_id" => 1, "slug" => str_slug("Indické", "-")]);
        	Type::create(['name' => "Ceylonské", "parent_id" => 1, "slug" => str_slug("Ceylonské", "-")]);
        	Type::create(['name' => "Rozličné", "parent_id" => 1, "slug" => str_slug("Rozličné", "-")]);
        	Type::create(['name' => "Čínské", "parent_id" => 1, "slug" => str_slug("Čínské", "-")]);
        	Type::create(['name' => "Tchajwanské", "parent_id" => 1, "slug" => str_slug("Tchajwanské", "-")]);
        	Type::create(['name' => "Vietnamské", "parent_id" => 1, "slug" => str_slug("Vietnamské", "-")]);
        	Type::create(['name' => "Japonské", "parent_id" => 1, "slug" => str_slug("Japonské", "-")]);
        	Type::create(['name' => "Aromatické", "parent_id" => 1, "slug" => str_slug("Aromatické", "-")]);
        	Type::create(['name' => "Ovocné", "parent_id" => 1, "slug" => str_slug("Ovocné", "-")]);
         	Type::create(['name' => "Rostlinné", "parent_id" => 1, "slug" => str_slug("Rostlinné", "-")]);
         	Type::create(['name' => "Cukrovinky", "parent_id" => 2, "slug" => str_slug("Cukrovinky", "-")]);
         	Type::create(['name' => "Konvice, šálky, soupravy", "parent_id" => 2, "slug" => str_slug("Konvice, šálky, soupravy", "-")]);
         	Type::create(['name' => "Ostatní čajové příslušenství", "parent_id" => 2, "slug" => str_slug("Ostatní čajové příslušenství", "-")]);
         	Type::create(['name' => "Čajové dózy", "parent_id" => 2, "slug" => str_slug("Čajové dózy", "-")]);
         	Type::create(['name' => "Knihy o čaji", "parent_id" => 2, "slug" => str_slug("Knihy o čaji", "-")]);
        	Type::create(['name' => "Bio a organic", "parent_id" => 2, "slug" => str_slug("Bio a organic", "-")]);
    }
}

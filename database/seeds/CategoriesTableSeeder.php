<?php

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->delete();

            Category::create(["name" => "Čaje", "slug" => str_slug("Čaje", "-")]);
            Category::create(["name" => "Ostatní", "slug" => str_slug("Ostatní", "-")]);
    }
}

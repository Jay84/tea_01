
/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
// require("jquery-match-height");

var VueResource = require("vue-resource");
window.Event = new Vue();

if (window.innerWidth < 768) {
    window.Show = false;
} else {
    window.Show = true;
}

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('buttons', require('./components/Buttons.vue'));

Vue.component('shop-main', require('./components/shop/Main.vue'));
Vue.component('shop-breadcrumb', require('./components/shop/Breadcrumb.vue'));
Vue.component('shop-sidebar', require('./components/shop/Sidebar.vue'));
Vue.component('subsections-filtr', require('./components/shop/SubsectionsFiltr.vue'));
Vue.component('shop-items', require('./components/shop/Items.vue'));
Vue.component('shop-item', require('./components/shop/Item.vue'));
Vue.component('pagination', require('./components/shop/Pagination.vue'));

Vue.component('checkout-main', require('./components/checkout/Main.vue'));
Vue.component('checkout-cart', require('./components/checkout/Cart.vue'));
Vue.component('checkout-cart-item', require('./components/checkout/CartItem.vue'));
Vue.component('checkout-shipping', require('./components/checkout/Shipping.vue'));
Vue.component('checkout-address-first', require('./components/checkout/FormAddressFirst.vue'));
Vue.component('checkout-address-second', require('./components/checkout/FormAddressSecond.vue'));

Vue.component('modal', require('./components/Modal.vue'));
Vue.component('user-menu', require('./components/UserMenu.vue'));

Vue.component('test-test', require('./components/Test.vue'));


const app = new Vue({
    el: '#app',
    data: {
    	queryingDatabase: false,
    },
    methods: {
        handleResize() {
            Event.$emit("resizing");
            if (window.innerWidth < 768) {
                Event.$emit("mobileWidth", true)
            } else {
                Event.$emit("mobileWidth", false)
            }
        }
    },
    mounted() {
        window.addEventListener("resize",this.handleResize);

    	// Event.$on("databaseQueryStart", () => {
    	// 	this.queryingDatabase = true;
    	// })

    	// Event.$on("databaseQueryEnd", () => {
    	// 	this.queryingDatabase = false;
     //        // window.scroll(0,0);
    	// })
    }
});
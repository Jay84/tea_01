<div class="row nav-wrapper">
	<div class="list-inline col-xs-12">
		<ul class="nav-primary hidden-xs pull-right">
			<li><a href="/maloobchod">VÝHODNÝ NÁKUP - REGISTRACE</a><span></span></li>
			<li><a href="">KONTAKTY</a><span></span></li>
			@if(Auth::guest())
			<li><a href="/login">PŘIHLÁSIT</a></li>
			@else
			<li><a href="/register">{{Auth::user()->name}}</a>
				<div class="nav-menu">
					<div class="arrow-up"></div>
					<ul class="nav-dropdown">
						<li><a href="/home">Informace</a></li>
						<li><a href="/historie">Objednávky</a></li>
						<li><a href="/logout">Odhlásit</a></li>
					</ul>
				</div>
			</li>
			@endif
		</ul>
	</div>
	<div class="col-xs-12">
		<div class="navigation-panel">
			<div class="nav-brand">
				<a href="/"><h1>ČAJE AMANA</h1></a>
			</div>
		</div>
	</div>
</div>

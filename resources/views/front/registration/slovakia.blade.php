@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-md-12">

    	@include("front.registration.partials.nav")

		<div class="panel-group custom-acordion" id="accordion" role="tablist" aria-multiselectable="true">
		  <div class="panel panel-default">
		    <div class="panel-heading" role="tab" id="headingOne">
		      <h4 class="panel-title">
		        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
		          Obchodní podmínky - Slovensko
		        </a>
		      </h4>
		    </div>
		    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
		      <div class="panel-body">
		        <h4>Toto jsou stručná pravidla obchodu s naší firmou :</h4>
		        <ul>
		        	<li>
		        		Všechny uvedené ceny jsou kalkulovány včetně příslušné DPH. Standardní množství jednoho druhu čaje určené k odběru je 1 kg. Zboží expedujeme do 3 pracovních dnů od data objednávky. Standardní výše velkoobchodní objednávky činí 2.000,- Kč od které hradíme dopravu či poštovné. Při odběru zboží v hodnotě nad 7.000,- Kč poskytujeme slevu ve výši 3 % z ceny zboží. O způsobu úhrady faktury a o dalších případných slevách rozhoduje vzájemná dohoda. Na požádání Vám rádi zdarma zašleme Vámi vybrané vzorky našich čajů.
		        	</li>
		        	<li>
		        		U volně sypaných druhů pravého čaje (včetně aromatisovaných čajů) je možné po dohodě jejich množství upravit. Čaje v cenách od 370 Kč do 1.000 Kč/kg lze objednat 0,5 kg, přičemž balné činí 48,- Kč za každé balení. Čaje v cenách od 1.000 Kč do 1.500 Kč/kg lze objednat 0,5 kg, nad 1.500,- Kč/kg pak 300 g (případně originální balení), přičemž v těchto případech naše společnost neúčtuje žádné balné. Čaje v cenách do 370 Kč/kg lze objednat v minimálním množství 1 kg. Pro čajové bonbony, kandovaný cukr a směsi koření platí stejné obchodní podmínky.
		        	</li>
		        	<li>
		        		U ostatních druhů volně sypaného zboží (ovocné a rostlinné čaje) je minimální množství jednoho druhu určené k odběru 1 kg.
		        	</li>
		        	<li>
		        		Čaje nabízené v tomto ceníku je možno obdržet za stejných odběrních podmínek též rozvážené po 100 g či 50 g do hliníkových sáčků se samolepící etiketou obsahující vedle názvu čaje také jeho popis (popřípadě složení) a přípravu. Cena balného za jeden sáček činí 15,- Kč.
		        	</li>
		        	<li>
		        		Vedle volně sypaných čajů nabízí naše společnost také široký výběr balených čajů a rozličného čajového příslušenství. Platí obdobné obchodní podmínky jako pro nákup čajů. Minimální množství balených čajů a dóz určené k prodeji je stanoveno na 3 kusy od jednotlivého druhu a velikosti bez ohledu na to, zda jsou dózy prázdné, či zda je v nich zabalen čaj.
		        	</li>
		        	<li>
		        		Poskytujeme Vám 20% slevu na veškeré zboží z maloobchodní části tohoto katalogu (vyžaduje samostatnou maloobchodní registraci), což Vám mimo jiné umožní nabídku a nákup menších množství čaje pro Vaše zákazníky či přímo pro Vás.
		        	</li>
		        	<li>
		        		Tento katalog byl vydán společností AMANA s.r.o., U pastoušky 159/1, Praha 5 - Jinonice, 158 00, fax: 251 613 639, tel: 251 612 100, 603 544 749 (ne SMS !), e-mail: amana@caj.cz. Zboží je možno objednat přímo v internetovém eshopu www.caj.cz , telefonicky (v pracovní dny od 10.00-17.00 hodin),e mailem či osobně (po předchozí dohodě) na uvedené adrese. Pokud budete mít jakékoli dotazy či připomínky k internetovému katalogu, kontaktujte nás na adrese info@caj.cz
		        	</li>
		        </ul>
		      </div>
		    </div>
		  </div>
		</div>

        <div class="panel panel-default">
            <div class="panel-heading">Registrace zákazníka ze Slovenska</div>

            <div class="panel-body">

            	@include("front.registration.partials.form")

            </div>
        </div>
    </div>
</div>
@endsection

<form action="/newcustomer" method="post" class="registration-form">
    <div class="col-sm-6">
        <div class="form-group {{$errors->has("company") ? 'has-error' : ''}} ">
            <label for="company">Obchodní jméno</label>
            @if($errors->has("company"))
                <small class="text-danger">{{$errors->first("company")}}</small>
            @endif
            <input type="text" id="company" name="company" class="form-control input-sm" value="{{old('company') ? old('company') : '' }}">
        </div>
        <div class="form-group">
            <label for="ico">IČO</label>
            <input type="text" id="ico" name="ico" class="form-control input-sm" value="{{old('ico') ? old('ico') : '' }}">
        </div>
        <div class="form-group">
            <label for="dic">DIČ</label>
            <input type="text" id="dic" name="dic" class="form-control input-sm" value="{{old('dic') ? old('dic') : '' }}">
        </div>
        <div class="form-group {{$errors->has("first_name") ? 'has-error' : ''}}">
            <label for="first_name">Jméno</label>
            @if($errors->has("first_name"))
                <small class="text-danger">{{$errors->first("first_name")}}</small>
            @endif
            <input type="text" id="first_name" name="first_name" class="form-control input-sm" value="{{old('first_name') ? old('first_name') : '' }}">
        </div>
        <div class="form-group {{$errors->has("last_name") ? 'has-error' : ''}}">
            <label for="last_name">Příjmení</label>
            @if($errors->has("last_name"))
                <small class="text-danger">{{$errors->first("last_name")}}</small>
            @endif
            <input type="text" id="last_name" name="last_name" class="form-control input-sm" value="{{old('last_name') ? old('last_name') : '' }}">
        </div>
        <div class="form-group {{$errors->has("address") ? 'has-error' : ''}}">
            <label for="address">Ulice, číslo</label>
            @if($errors->has("address"))
                <small class="text-danger">{{$errors->first("address")}}</small>
            @endif
            <input type="text" id="address" name="address" class="form-control input-sm" value="{{old('address') ? old('address') : '' }}">
        </div>
        <div class="form-group {{$errors->has("city") ? 'has-error' : ''}}">
            <label for="city">Město</label>
            @if($errors->has("city"))
                <small class="text-danger">{{$errors->first("city")}}</small>
            @endif
            <input type="text" id="city" name="city" class="form-control input-sm" value="{{old('city') ? old('city') : '' }}">
        </div>
        <div class="form-group {{$errors->has("psc") ? 'has-error' : ''}}">
            <label for="psc">PSČ</label>
            @if($errors->has("psc"))
                <small class="text-danger">{{$errors->first("psc")}}</small>
            @endif
            <input type="text" id="psc" name="psc" class="form-control input-sm" value="{{old('psc') ? old('psc') : '' }}">
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group {{$errors->has("phone") ? 'has-error' : ''}}">
            <label for="phone">Telefon</label>
            @if($errors->has("phone"))
                <small class="text-danger">{{$errors->first("phone")}}</small>
            @endif
            <input type="text" id="phone" name="phone" class="form-control input-sm" value="{{old('phone') ? old('phone') : '' }}">
        </div>
        <div class="form-group">
            <label for="fax">Fax</label>
            <input type="text" id="fax" name="fax" class="form-control input-sm" value="{{old('fax') ? old('fax') : '' }}">
        </div>
        <div class="form-group {{$errors->has("email") ? 'has-error' : ''}}">
            <label for="email">Email</label>
            @if($errors->has("email"))
                <small class="text-danger">{{$errors->first("email")}}</small>
            @endif
            <input type="text" id="email" name="email" class="form-control input-sm" value="{{old('email') ? old('email') : '' }}">
        </div>
        <div class="form-group">
            <label for="email_confirmation">Email, znovu</label>
            <input type="text" id="email_confirmation" name="email_confirmation" class="form-control input-sm">
        </div>
        <div class="form-group">
            <label for="www">WWW</label>
            <input type="text" id="www" name="www" class="form-control input-sm" value="{{old('www') ? old('www') : '' }}">
        </div>
        <div class="form-group {{$errors->has("name") ? 'has-error' : ''}}">
            <label for="name">Uživatelské jméno</label>
            @if($errors->has("name"))
                <small class="text-danger">{{$errors->first("name")}}</small>
            @endif
            <input type="text" id="name" name="name" class="form-control input-sm" value="{{old('name') ? old('name') : '' }}">
        </div>
        <div class="form-group {{$errors->has("password") ? 'has-error' : ''}}">
            <label for="password">Heslo</label>
            @if($errors->has("password"))
                <small class="text-danger">{{$errors->first("password")}}</small>
            @endif
            <input type="password" id="password" name="password" class="form-control input-sm">
        </div>
        <div class="form-group">
            <label for="password_confirmation">Heslo, znovu</label>
            <input type="password" id="password_confirmation" name="password_confirmation" class="form-control input-sm">
        </div>
    </div>
    <div class="col-sm-12">
        <div class="form-group">
            <label for="message">Vaše poznámky</label>
            <textarea name="message" id="message" class="form-control">{{old('message') ? old('message') : '' }}</textarea>
        </div>
        <div class="form-group novinky">
          <input type="checkbox" id="news" name="news" value="true" checked>
          <label for="news">Souhlasím se zasíláním novinek</label>
        </div>
        <input type="hidden" name="customer_type" value="{{$customer_type}}">
        <div class="form-group">
          <input type="submit" class="clickable submit-btn pull-right">
        </div>
    </div>
  {{csrf_field()}}
</form>
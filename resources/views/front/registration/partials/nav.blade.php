<ul class="nav nav-tabs registration">
	<li role="presentation"><a href="/maloobchod">Maloobchod</a></li>
	<li role="presentation"><a href="/velkoobchod">Velkoobchod</a></li>
	<li role="presentation"><a href='/slovensko'>Slovensko</a></li>
</ul>

@section("scripts")
<script>
	$('a[href="' + window.location.pathname + '"]').parent().addClass("active");
</script>
@endsection
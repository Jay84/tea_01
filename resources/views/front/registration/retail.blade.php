@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-md-12">

    	@include("front.registration.partials.nav")

		<div class="panel-group custom-acordion" id="accordion" role="tablist" aria-multiselectable="true">
		  <div class="panel panel-default">
		    <div class="panel-heading" role="tab" id="headingOne">
		      <h4 class="panel-title">
		        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
		          Obchodní podmínky - Maloobchod
		        </a>
		      </h4>
		    </div>
		    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
		      <div class="panel-body">
		        <h4>Toto jsou stručná pravidla obchodu s naší firmou :</h4>
		        <ul>
		        	<li>
		        		Všechny uvedené ceny jsou kalkulovány <strong>včetně příslušné DPH</strong>.
		        	</li>
		        	<li>
		        		Zboží expedujeme do 3 pracovních dnů od data objednávky prostřednictvím kurýrní služby GLS nebo Českou poštou. 
		        	</li>
		        	<li>
		        		Poštovné za zásilku <strong>GLS činí 99,- Kč</strong>. Poštovné za zásilku <strong>Českou poštou 149,- Kč</strong>.
		        	</li>
		        	<li>
		        		Při <strong>objednávce nad 900,- Kč </strong>hradíme poštovné při odeslání kurýrem GLS. U objednávek nad 900,- Kč zasílaných Českou poštou účtujeme poštovné 50,- Kč.
		        	</li>
		        	<li>
		        		Při objednávce nad 1.000,- Kč <strong>můžete objednat vzorek čaje zdarma</strong> dle Vašeho výběru.
		        	</li>
		        	<li>
		        		Při objednávce zboží v hodnotě nad 1500,- Kč si můžete vybrat buď <strong>3 vzorky různých čajů, nebo slevu ve výši 5 % z ceny zboží</strong>. Slevu a vzorky zdarma nelze zkombinovat.
		        	</li>
		        	<li>
		        		Hmotnost vzorku zdarma je 10g, u čajů v ceně nad 500,- Kč/100g pak 5g.
		        	</li>
		        	<li>
		        		Všechny uvedené ceny jsou kalkulovány včetně příslušné DPH.
		        	</li>
		        </ul>
		        <h4>Pro zasílání zboží na Slovensko platí stejné obchodní podmínky s těmito úpravami:</h4>
		        <ul>
		        	<li>
		        		Zboží zasíláme na dobírku v EUR, cena je přepočítána z ceny v CZK kurzem 25 Kč/EUR.
		        	</li>
		        	<li>
		        		Poštovné hradíme při objednávce nad 3000,- Kč, výše poštovného činí 190,- Kč.
		        	</li>
		        </ul>
		      </div>
		    </div>
		  </div>
		</div>

        <div class="panel panel-default">
            <div class="panel-heading">Registrace Maloobchodního zákazníka</div>

            	<div class="panel-body">

		            @include("front.registration.partials.form")

            	</div>

        </div>
    </div>
</div>
@endsection

@extends('layouts.app')

@section("notifications")
	@if(Session::has("purchase"))
		<modal></modal>
	@endif
@endsection

@section('content')
		{{-- {{dd(Session::all())}} --}}
        <shop-main sidebar_items="{{ json_encode($sidebar_items)}}"></shop-main>

@endsection

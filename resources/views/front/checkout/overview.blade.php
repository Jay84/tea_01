@extends('layouts.app')

@section('content')

      <div class="row">
          
        <div class="col-sm-12">
			
			<checkout-main cartjson="{{json_encode($input)}}"></checkout-main>
			
        </div>
        
      </div>

@endsection

@section("scripts")
	<script type="text/javascript">
	    $(function () {
	        $('[data-toggle="tooltip"]').tooltip();   
	    });
	</script>
@endsection

@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-md-12">

        @include("user.partials.nav")

        

        <div class="panel panel-default">
            <div class="panel-heading">Historie objednávek</div>

                <div class="panel-body">
					@foreach($purchases as $purchase)
					<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
					 
					  <div class="panel panel-default">
					    <div class="panel-heading" role="tab" id="heading{{$purchase->first()->id}}">
					      <h4 class="panel-title">
					        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$purchase->first()->id}}" aria-expanded="true" aria-controls="collapse{{$purchase->first()->id}}">
					          {{$purchase->first()->created_at->diffForHumans()}}
					        </a>
					      </h4>
					    </div>
					    <div id="collapse{{$purchase->first()->id}}" class="panel-collapse collapse " role="tabpanel" aria-labelledby="heading{{$purchase->first()->id}}">
					      <div class="panel-body">
					        <table class="table">
					        	<thead>
					        		<tr>
					        			<th>Produkt</th>
					        			<th>Jednotka</th>
					        			<th>Počet</th>
					        			<th>Cena</th>
					        		</tr>
					        	</thead>
					        	<tbody>
					        	@foreach($purchase as $value)
					        		<tr>
					        			<td> {{$value->item->name}} </td>
					        			<td> {{$value->unit}} </td>
					        			<td> {{$value->quantity}} </td>
					        			<td> {{$value->price}} </td>
					        		</tr>
					        	@endforeach
					        	</tbody>
					        </table>
					      </div>
					    </div>
					  </div>
					  
					</div>
					@endforeach
                    
                </div>

        </div>
    </div>
</div>
@endsection

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link href="/css/app.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>
    <div id="app">
        <div class="background-image"></div>
            {{-- <img src="img/tealeaf.jpg" alt=""> --}}
        <div class="container hidden">

            @yield("notifications")

            @include("partials.navigation")

            <buttons v-bind:cart_items_count={{$cart_items_count}}></buttons>  
            
            <user-menu v-bind:guest={{Auth::guest() ? "true" : "false"}} ></user-menu>

            @yield('content')

        </div>
        
    </div>

    <!-- Scripts -->
    <script src="/js/app.js"></script>
    <script>
        $(document).ready(function() {
            $(".container").removeClass("hidden");
        });
    </script>
    {{-- <script src="/js/jquery.matchHeight.js" type="text/javascript"></script> --}}
    @yield("scripts")
</body>
</html>
